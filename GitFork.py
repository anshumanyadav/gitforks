# -*- coding: utf-8 -*-
"""
Created on Mon Nov  9 16:52:23 2020

@author: ADMIN
"""

from github import Github
import numpy as np
import sys




# m = 10

# top_m = np.array(l[:m])[:m][:,1]
# top_m_repos = []
# for i in top_m:
#     top_m_repos.append(org.get_repo(i))



class gitobj:
    def __init__(self, organisation, n, m):
        self.organisation = organisation
        self.n = n
        self.m = m
        self.git = Github("4aa585942982b721f68c898159ff269e6e38d6f6")
        
    def get_top_n_organization(self):
        #settng up the organisation
        org = self.git.get_organization("google")
        t = org.get_repos()
        d = dict()
        #getting forks for each repo in a dictionary
        for i in t:
            d[i.name] = i.forks
        #reverse sorting on number of forks 
        l = []
        for i in d.keys():
            l.append([d[i],i])
        l.sort(reverse = True)
        
        m = self.m
        n = self.n
        top_m = np.array(l[:m])[:m][:,1]
        top_m_repos = []
        #getting top n from the repos
        print("The top n Repositories are : -")
        for i in top_m:
            top_m_repos.append(org.get_repo(i))
            print(i)
        #getting contributors for each repo
        print("For each Organisation the top m contributors are : -")
        for k in range(len(top_m_repos)):
            contrib = top_m_repos[k].get_stats_contributors()
            d = []
            for i in contrib:    
                if(i.author!=None):
                    d.append([i.total,i.author.login])
            #reverse sorting each contributor and taking top n of those
            d.sort(reverse = True)
            top_n_contributors = d[:n]
            print("For" +" "+ str(top_m_repos[k].name) + " the top m comitees are : -")
            for i in top_n_contributors:
                print("Contributor - " + i[1] + " No. of commits = " + str(i[0]))
            print(" ")
            print(' ')
        return
    

        
        
        

            
def main():
    try:
        organisation = sys.argv[1]
    except:
        print("Enter name of Organisation : - ")
        organisation = input()
    try:
        n = int(sys.argv[2])
    except:
        print("Enter Number of Top repositores(n) : -")
        n = int(input())
    try:
        m = int(sys.argv[3])
    except:
        print("Enter Number of Top comiteers(m) : -")
        m = int(input())
    git_object = gitobj(organisation,n,m)
    git_object.get_top_n_organization()
    
    
    
    

if __name__== '__main__':
	main()